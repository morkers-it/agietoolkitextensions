<?php

namespace Agiletoolkitextension;

use atk4\ui\jsChain;

class jsTabulator extends jsChain
{
    /**
     * Params for this constructior will be passed on to jQuery() in JavaScript.
     * Start with: jsExpressionable|View|string $selector such as '.myclass' or $view.
     * Second argument would be $context. See jQuery manual for more info.
     *
     * @param array $constructorArgs - passes $selector and $context to jQuery(..)
     */
    public function __construct($varNamr, $target)
    {
        parent::__construct();
        $this->_library = $varNamr;
        $this->_constructorArgs = [$target];
    }

    /**
     * Produce String representing this JavaScript extension.
     *
     * @return string
     */
    public function jsRender(): string
    {
        $ret = 'var ';

        // start with constructor
        $ret .= $this->_library;

        $ret .= '=new Tabulator';

        // next perhaps we have arguments
        if ($this->_constructorArgs) {
            $ret .= $this->_renderArgs($this->_constructorArgs);
        }

        // next we do same with the calls
        foreach ($this->_chain as $chain) {
            if (is_array($chain)) {
                $ret .= '.'.$chain[0].$this->_renderArgs($chain[1]);
            } elseif (is_numeric($chain)) {
                $ret .= '['.$chain.']';
            } else {
                $ret .= '.'.$chain;
            }
        }

        return $ret;
    }
}
