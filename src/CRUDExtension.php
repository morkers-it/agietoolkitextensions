<?php

namespace Agiletoolkitextension;

use Atk4\Data\Model;
use Atk4\Ui\App;
use Atk4\Ui\Button;
use Atk4\Ui\Callback;
use Atk4\Ui\Crud;
use Atk4\Ui\Exception;
use Atk4\Ui\jQuery;
use Atk4\Ui\jsCallback;
use Atk4\Ui\jsExpression;
use Atk4\Ui\jsFunction;
use Atk4\Ui\jsModal;
use Atk4\Ui\Modal;
use Atk4\Ui\Table\Column\ActionButtons;
use Atk4\Ui\View;
use Atk4\Ui\VirtualPage;
use Atk4\Ui\UserAction\ExecutorInterface;

class CRUDExtension extends Crud
{
    const CALCULATION_PLACEMENT_TOP    = 'top';
    const CALCULATION_PLACEMENT_BOTTOM = 'bottom';

    const CALCULATION_FUNCTION_AVERAGE     = 'avg';
    const CALCULATION_FUNCTION_MAXIMUM     = 'max';
    const CALCULATION_FUNCTION_MINIMUM     = 'min';
    const CALCULATION_FUNCTION_SUM         = 'sum';
    const CALCULATION_FUNCTION_CONCATENATE = 'concat';
    const CALCULATION_FUNCTION_COUNT       = 'count';

    public $defaultTemplate = __DIR__ . '/../templates/crud_extension.html';

    public $tabulatorOptions = [
        'columns' => [],
        'placeholder' => 'No data!',
        'ajaxLoaderLoading' => '<div class="ui active inverted dimmer"><div class="ui text loader">Loading...</div></div>',
//        'ajaxLoaderLoading' => 'Loading...',
        'ajaxLoader' => true,
        'virtualDom' => true,
        'headerFilterPlaceholder' => 'Filtrēt ....',
    ];

    public $tablePlaceholder;

    public $dataLoaderCb;

    public $tableAction = [];

    public $quickSearchFields = [];

    public $lowercasePrefix = 'LWCS_';

    public $dynamicColumns = [];

    public $emptyHeaderFilterValue = 'NAV';

    public $actionColumnTitle = 'Actions';

    public $canCreate = true;

    public $canUpdate = true;

    public $canDelete = true;

    /**
     * @var jsTabulator
     */
    public $jsTabulator;

    public static function addTabulatorResources(App $app)
    {
        $app->requireCSS('../assets/tabulator_semantic-ui.min.css');
        $app->requireJS('../assets/tabulator.js');
    }

    protected function init(): void
    {
        parent::init();
        if (!$this->getOwner() instanceof VirtualPage) {
            $this->getApp()->requireCSS('../assets/tabulator_semantic-ui.min.css');
            $this->getApp()->requireJS('../assets/tabulator.js');
        }

        $this->container->removeElement($this->table->short_name);
        $this->container->removeElement('view');
        $this->paginator = null;
//        $this->tablePlaceholder = $this->container->add(['AbstractView'], 'table_placeholder');
        $this->tablePlaceholder = View::addTo($this->container, ['region' => 'table_placeholder']);
        $this->tablePlaceholder->class[] = 'celled';
        $this->tablePlaceholder->class[] = 'striped';
        $this->jsTabulator      = new jsTabulator($this->name, $this->tablePlaceholder);
    }

    public function reloadData()
    {
        return new jsExpression($this->getTabulatorVariable() . '.replaceData()');
    }

    public function updateData(array $data)
    {
        return new jsExpression($this->getTabulatorVariable() . '.updateData([])', [$data]);
    }

    public function addJsPaginatorInContainer($ipp, $height, $options = [], $container = null, $scrollRegion = 'Body')
    {
        $cb = Callback::addTo($this->tablePlaceholder);
        $cb->set(function ($page, $sorters, $filters) use ($ipp) {
            //apply filters
            $conditions = [];

            foreach ($filters as $filter) {
                if (!$filter['field']) {
                    $quickSearchConditions = [];
                    foreach ($this->quickSearchFields as $field) {
                        if ($this->model->hasElement($this->lowercasePrefix . $field)) {
                            $quickSearchConditions[] = [$this->lowercasePrefix . $field, 'like', '%' . strtolower($filter['value']) . '%'];
                        } else {
                            $quickSearchConditions[] = [$field, 'like', '%' . $filter['value']. '%'];
                        }
                    }
                    $scope = Model\Scope::createOr();
                    foreach ($quickSearchConditions as $condition) {
                        call_user_func_array([$scope, 'addCondition'], $condition);
                    }
                    $this->model->addCondition($scope);
                } else {
                    if ($this->model->hasElement($this->lowercasePrefix . $filter['field'])) {
                        $conditions[] = [$this->lowercasePrefix . $filter['field'], 'like', '%' . strtolower($filter['value']) . '%'];
                    } else {
                        $conditions[] = [$filter['field'], 'like', '%' . $filter['value'] . '%'];
                    }
                }
            }

            foreach ($conditions as $condition) {
                call_user_func_array([$this->model, 'addCondition'], $condition);
            }

            //Detect last page
            $count    = $this->model->action('count')->getOne();
            $lastPage = $count / $ipp;

            if($lastPage - (int) $lastPage > 0) {
                $lastPage++;
            }

            //Set page limits
            $this->model->setLimit($ipp, ($page - 1) * $ipp);

            //add apply sorters
            foreach ($sorters as $sorter) {
                $this->model->setOrder($sorter['field'], $sorter['dir']);
            }

            //Build actions row is actions present
            $dataSet = [];
            $html    = '';

            if (count($this->actionButtons->buttons)) {
                /** @var Button $button */
                foreach ($this->actionButtons->buttons as $button) {
                    $button->setAttr('data-id', '__id__');
                    $html .= $button->getHTML();
                }
            }

            foreach ($this->model->rawIterator() as $item) {
                if ($html != '') {
                    $item['actions'] = strtr($html, ['__id__' => $item[$this->model->id_field]]);
                }

                foreach ($this->dynamicColumns as $columnName => $function) {
                    $item[$columnName] = $function($item);
                }

                foreach($item as $key => $value) {
                    if (is_resource($value)) {
                        $item[$key] = stream_get_contents($value);
                    }
                }

                $dataSet[] = $item;
            }

            $this->getApp()->terminateJson(json_encode(['data' => $dataSet, 'last_page' => (int)$lastPage]));
        }, [
            $_GET['page'] ?? 1,
            $_GET['sorters'] ?? [],
            $_GET['filters'] ?? [],
        ]);

        $url = $this->jsURL();
        $parts = explode('?', $url);
        $url = $parts[0];
        $baseArgs = [];

        if (isset($parts[1])) {
            $args = explode('&', $parts[1]);
            $baseArgs = array_reduce($args, function($ac, $arg) {
                $argParts = explode('=', $arg);
                $ac[$argParts[0]] = $argParts[1] ?? true;

                return $ac;
            }, []);
        }

        $this->ipp = $ipp;
        // $this->table->addJsPaginator($ipp);
        $this->tabulatorOptions = array_merge($this->tabulatorOptions, [
            'ajaxProgressiveLoad' => 'scroll',
            'paginationSize'      => $ipp,
            'height'              => is_int($height) ? $height . 'px' : $height,
            'ajaxURL'             => $url,
            'ajaxParams'          => array_merge([
                $cb->name        => 'ajax',
                '__atk_callback' => 1,
            ], $baseArgs),
            'ajaxSorting'         => true,
            'layout'              => 'fitColumns',
            'ajaxFiltering' => true,
        ]);
    }

    public function addAction($button, $action, $confirm = false)
    {
        if (!$this->actionButtons) {
            $this->actionButtons = new ActionButtons();
        }

        $name = $this->name . '_action_' . (count($this->actionButtons->buttons) + 1);

        if (!is_object($button)) {
            $button = new Button($button);
        }
        $button->setApp($this->getApp());

        $this->actionButtons->buttons[$name] = $button;
        $button->addClass('b_' . $name);
        $button->addClass('compact');
        $this->tablePlaceholder->on('click', '.b_' . $name, $action,
            [(new jQuery())->data('id'), 'confirm' => $confirm]);
        $this->tablePlaceholder->add($button);
    }

    /**
     * Similar to addAction but when button is clicked, modal is displayed
     * with the $title and $callback is executed through VirtualPage.
     *
     * @param string|array|View $button
     * @param string            $title
     * @param callable          $callback function($page){ . .}
     */
    public function addModalAction($button, $title, $callback, $args = [])
    {
        return $this->addModal($button, $title, $callback, $this);
    }

    public function renderAll(): void
    {
        if (count($this->actionButtons->buttons)) {
            $this->tabulatorOptions['columns'][] = [
                'title'     => $this->actionColumnTitle,
                'field'     => 'actions',
                'formatter' => 'html',
                'headerSort' => false,
            ];
        }

        $this->tabulatorOptions['columns'] = array_values($this->tabulatorOptions['columns']);
        $this->js(true, new jsExpression($this->getTabulatorVariable() . ' = new Tabulator([],[])',
            [$this->tablePlaceholder, $this->tabulatorOptions]));
//        $this->js(true, $this->jsTabulator);
        parent::renderAll();
    }

    public function setModel(\atk4\data\Model $m, $defaultFields = null): Model
    {
        parent::setModel($m, $defaultFields);

        if (is_array($this->displayFields)) {
            foreach ($this->displayFields as $field) {
                $this->addColumn($field);
            }
        } else {
            foreach ($m->getFields() as $name => $element) {
                if (!$element instanceof \atk4\data\Field) {
                    continue;
                }

                if ($element->isVisible()) {
                    $this->tabulatorOptions['columns'][$name] = [
                        'title' => $element->getCaption(),
                        'field' => $name,
                    ];
                }
            }
        }
        
        // Filter buttons based on can properties
        $this->actionButtons->buttons = array_filter($this->actionButtons->buttons, function(Button $button) {
            switch($button->icon) {
                case 'edit':
                    return $this->canUpdate;
                case 'red trash':
                    return $this->canDelete;
                default:
                    return true;
            }

            return false;
        });

        return $this->model;
    }

    public function addExecutorButton(ExecutorInterface $executor, Button $button = null)
    {
        $btn = $button ? $this->add($button) : $this->getExecutorFactory()->createTrigger($executor->getAction(), $this->getExecutorFactory()::TABLE_BUTTON);

        if (!$this->actionButtons) {
            $this->actionButtons = $this->table->addColumn(null, $this->actionButtonsDecorator);
        }

        $this->addAction($btn, $executor);

        return $button;
    }

    public function addColumn($name, $columnDecorator = null, $field = null)
    {
        if (!$this->_initialized) {
            throw new Exception\NoRenderTree($this, 'addColumn()');
        }

        if (!$this->model) {
            $this->model = new \Atk4\Ui\Misc\ProxyModel();
        }

        // This code should be vaugely consistent with Form\Layout::addControl()

        if (is_string($field)) {
            $field = ['type' => $field];
        }

        if ($name === null) {
            // table column without respective field in model
            $field = null;
        } elseif (!$this->model->hasField($name)) {
            $field = $this->model->addField($name, $field);

            $field->never_persist = true;
        } else {
            $existingField = $this->model->getField($name);

            if (is_array($field)) {
                $field = $existingField->setDefaults($field);
            } elseif (is_object($field)) {
                throw (new Exception('Duplicate field'))
                    ->addMoreInfo('name', $name);
            } else {
                $field = $existingField;
            }
        }

        if ($name === null) {
            $this->columns[] = $columnDecorator;
        } elseif (!is_string($name)) {
            throw (new Exception('Name must be a string'))
                ->addMoreInfo('name', $name);
        } elseif (isset($this->columns[$name])) {
            throw (new Exception('Table already has column with $name. Try using addDecorator()'))
                ->addMoreInfo('name', $name);
        } else {
            $this->tabulatorOptions['columns'][$name] = [
                'field' => $name,
                'title' => $field->getCaption(),
            ];
        }

        return $columnDecorator;
    }

    public function addDynamicColumn($name, $caption, $function)
    {
        $this->tabulatorOptions['columns'][$name] = [
            'field' => $name,
            'title' => $caption,
            'headerSort' => false,
        ];
        $this->dynamicColumns[$name] = $function;
    }

    /**
     * Initializes interface elements for editing records.
     *
     * @return array|jsExpression
     */
    public function initUpdate()
    {
        $this->addAction(['icon' => 'edit'], new jsModal('Edit', $this->pageUpdate,
            [$this->name => (new jQuery())->data('id'), $this->name . '_sort' => $this->getSortBy()]));

        $this->pageUpdate->set(function () {
            $this->model->load($this->getApp()->stickyGet($this->name));

            // Maybe developer has already created form
            if (!is_object($this->formUpdate) || !$this->formUpdate->_initialized) {
                $this->formUpdate = $this->pageUpdate->add($this->formUpdate ? : $this->formDefault);
            }

            $this->formUpdate->setModel($this->model, $this->fieldsUpdate ? : $this->fieldsDefault);

            if ($sortBy = $this->getSortBy()) {
                $this->formUpdate->stickyGet($this->name . '_sort', $sortBy);
            }

            // set save handler with reload trigger
            // adds default submit hook if it is not already set for this form
            if (!$this->formUpdate->hookHasCallbacks('submit')) {
                $this->formUpdate->onSubmit(function ($form) {
                    $form->model->save();

                    return $this->jsSaveUpdate();
                });
            }
        });
    }

    public function jsSave($notifier)
    {
        return [
            // close modal
            new jsExpression('$(".atk-dialog-content").trigger("close")'),

            // display notification
            $this->factory($notifier),

            // reload Grid Container.
            new jsExpression($this->getTabulatorVariable() . '.replaceData();'),
        ];
    }

    /**
     * Initialize UI for deleting records.
     */
    public function initDelete()
    {
        $this->addAction(['icon' => 'red trash'], function ($jschain, $id) {
            $this->model->load($id)->delete();

            return $jschain->closest('.tabulator-row')->transition('fade left');
        }, 'Are you sure?');
    }

    /**
     * Makes rows of this grid selectable by creating new column on the left with
     * checkboxes.
     *
     * @return TableColumn\CheckBox
     */
    public function addSelection()
    {
        $this->tabulatorOptions['selectable'] = true;

        return new jsExpression($this->getTabulatorVariable() . '.getSelectedData()');
    }

    public function addQuickSearch($fields = [], $hasAutoQuery = false)
    {
        $this->tabulatorOptions['ajaxFiltering'] = true;

        if (!$this->model) {
            throw new Exception(['Call setModel() before addQuickSearch()']);
        }

        if (!$this->menu) {
            throw new Exception(['Unable to add QuickSearch without Menu']);
        }

        if (!$fields) {
            $fields = [$this->model->title_field];
        }

        $this->quickSearchFields = $fields;

        if (!$this->menu) {
            throw new Exception(['Unable to add QuickSearch without Menu']);
        }

        $viewItem = $this->menu
            ->addMenuRight()->addItem()->setElement('div');
        $view = View::addTo($viewItem);

        $this->quickSearch = $view->add(new TabulatorSearch($this->getTabulatorVariable()));
    }

    public function setTabulatorOption($option, $value)
    {
        $this->tabulatorOptions[$option] = $value;
    }

    public function headerFilter(array $columns)
    {
        foreach ($columns as $column) {
            if (!isset($this->tabulatorOptions['columns'][$column])) {
                throw new Exception(sprintf('Column "%s" not found!', $column));
            }
            $this->tabulatorOptions['columns'][$column]['headerFilter'] = 'input';
        }
    }

    public function headerSelectFilter($column, $function, $useCookie = false)
    {
        if (!isset($this->tabulatorOptions['columns'][$column])) {
            throw new Exception(sprintf('Column "%s" not found!', $column));
        }

        /** @var jsCallback $cb */
        $cb = $this->tablePlaceholder->add([jsCallback::class]);
        $cb->set($function);

        $this->tabulatorOptions['columns'][$column]['headerFilter'] = 'remoteAutocomplete';
        $this->tabulatorOptions['columns'][$column]['headerFilterParams'] = [
            'url' => $cb->getJSURL(),
            'empty' => $this->emptyHeaderFilterValue,
        ];

        if ($useCookie) {
            $this->tabulatorOptions['columns'][$column]['headerFilterParams']['cookieKey'] = $this->name . '_' . $column;
        }

        $this->tabulatorOptions['columns'][$column]['headerFilterLiveFilter'] = false;
    }

    public function getTabulatorVariable()
    {
        return sprintf('window.%s', $this->tablePlaceholder->name);
    }

    public function addCalculation(
        $column,
        $function = self::CALCULATION_FUNCTION_AVERAGE,
        $placement = self::CALCULATION_PLACEMENT_BOTTOM
    ) {
        if (!isset($this->tabulatorOptions['columns'][$column])) {
            throw new Exception(sprintf('Column "%s" not found!', $column));
        }


        if (is_callable($function)) {
            /** @var Callback $cb */
            $cb = $this->tablePlaceholder->add('jsCallback');
            $cb->set($function);
            $this->tabulatorOptions['columns'][$column][$placement . 'Calc'] = new jsFunction(['values', 'data', 'calcParams'], [
                new jsExpression('if (!values.length){return;}'),
                new jsExpression('if (!window.calcCache){window.calcCache = new Object();}'),
                new jsExpression('if (window.calcCache.' . $column . '){return window.calcCache.' . $column .'}'),
                new jsExpression('window.calcCache.' . $column . ' = $.ajax({type:\'post\',' .
                    ' url:[],' .
                    ' data: {filters: ' . $this->getTabulatorVariable() . '.getFilters(), headerFilters: ' . $this->getTabulatorVariable() . '.getHeaderFilters()},' .
                    ' async: false}).responseText', [$cb->getJSURL()]),
                new jsExpression('return window.calcCache.' . $column),
            ]);

        } else {
            $this->tabulatorOptions['columns'][$column][$placement . 'Calc'] = $function;
        }
    }

    public function setColumnAsHtml($column)
    {
        if (!isset($this->tabulatorOptions['columns'][$column])) {
            throw new Exception(sprintf('Column "%s" not found!', $column));
        }

        $this->tabulatorOptions['columns'][$column]['formatter'] = 'html';
    }

    private function addModal($button, $title, $callback, $owner = null)
    {
        /** @var Modal $modal */
        $modal = $this->add([Modal::class, 'title' => $title]);
        $modal->observeChanges(); // adds scrollbar if needed
        $modal->set(function ($t) use ($callback) {
            call_user_func($callback, $t, $this->getApp()->stickyGet($this->name));
        });

        if (!is_object($button)) {
            $button = new Button($button);
        }

        $action = $this->addAction($button, $modal->show([$this->name => (new jQuery())->attr('id')]));

        return $action;
    }
}
