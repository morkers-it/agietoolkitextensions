<?php
/**
 * A Search input field that will reload View
 * using the view->url with a _q arguments attach to url.
 */

namespace Agiletoolkitextension;

use atk4\ui\jQuery;
use atk4\ui\jsExpression;
use atk4\ui\jsFunction;
use atk4\ui\View;

class TabulatorSearch extends View
{
    /**
     * The View to reload using this jsSearch.
     *
     * @var View
     */
    public $reload = null;

    public $args = [];

    /**
     * Whether or not jsSearch will query server on each keystroke.
     * Default is with using Enter key.
     *
     * @var bool
     */
    public $autoQuery = false;

    /**
     * The input field.
     *
     * @var FormField\Line
     */
    public $placeHolder = 'Search';

    public $defaultTemplate = 'js-search.html';

    /** @var string ui css classes */
    public $button = 'ui mini transparent basic button';
    public $filterIcon = 'filter';
    public $btnSearchIcon = 'search';
    public $btnRemoveIcon = 'red remove';
    public $btnStyle = null;
    public $tabulatorVar;

    public function __construct(string $tabulatorVar)
    {
        $this->tabulatorVar = $tabulatorVar;
    }

    protected function init(): void
    {
        parent::init();

//        $this->input = $this->add(new \atk4\ui\FormField\Line(['iconLeft' => 'filter',  'action' => new \atk4\ui\Button(['icon' => 'search', 'ui' => 'button atk-action'])]));
    }

    protected function renderView(): void
    {
        if ($this->placeHolder) {
            $this->template->trySet('Placeholder', $this->placeHolder);
        }

        if ($this->btnStyle) {
            $this->template->trySet('button_style', $this->btnStyle);
        }

        $this->template->set('Button', $this->button);
        $this->template->set('FilterIcon', $this->filterIcon);
        $this->template->set('BtnSearchIcon', $this->btnSearchIcon);
//        $this->template->set('BtnRemoveIcon', $this->btnRemoveIcon);

//        $function =  new JsFunction(['value'], [
//            new jsExpression('if (value) {'),
//            new jsExpression($this->tabulatorVar . '.setFilter(\'\',\'like\', value)'),
//            new jsExpression('} else {'),
//            new jsExpression($this->tabulatorVar . '.removeFilters()'),
//            new jsExpression('}()')
//        ]);
//        $function->a

        $this->js('change', new jsExpression(
            sprintf('%s.setFilter(\'\', \'like\',[])', $this->tabulatorVar),
            [(new jQuery())->find('input')->val()])
        , 'input');

        parent::renderView();
    }
}
