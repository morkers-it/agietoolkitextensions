<?php

namespace Agiletoolkitextension\Field;

use Atk4\Ui\Form\Control\Textarea;
use atk4\ui\jsExpression;
use atk4\ui\jsFunction;

/**
 * Class RitchText
 *
 * @package Agiletoolkitextension\Field
 */
class RichText extends Textarea
{
    public $onChange;

    public $tinyOptions = [
        'width' => '100%',
    ];

    protected function init(): void
    {
        parent::init();

        $this->tinyOptions['selector'] = '#' . $this->name . '_input';
        $this->tinyOptions['setup'] = new jsFunction(['editor'], [
            new jsExpression('editor.on("change",[])', [
                new jsFunction([], [
                    new jsExpression('editor.save()'),
                ]),
            ]),
            $this->onChange,
        ]);
        $this->js(true, new jsExpression('tinymce.remove([]); tinymce.init([])', [$this->tinyOptions['selector'], $this->tinyOptions]));
        $this->getApp()->requireJS('https://unpkg.com/tinymce@5.0.4/tinymce.js');
    }
}
