<?php

declare(strict_types=1);

namespace Atk4\Ui\Demos;

// to use MySQL database:
//   1. copy this file to "db.php"
//   2. uncomment the line below (and update the configuration if needed)
//   3. remove the Sqlite code from the new file
$db = new \Atk4\Data\Persistence\Sql('mysql:dbname=ag;host=ag-db', 'root', 'root');
