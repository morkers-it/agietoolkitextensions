<?php
namespace Atk4\Ui\Demos;
use Agiletoolkitextension\CRUDExtension;
/** @var \Atk4\Ui\App $app */
require_once __DIR__ . '/init-app.php';

//$app->add(['Button', 'Dynamic scroll in CRUD and Grid', 'small left floated basic blue', 'icon' => 'left arrow'])
//    ->link(['scroll-grid']);
$app->add([\Atk4\Ui\View::class, 'ui' => 'ui clearing divider']);
$header = $app->add([\Atk4\Ui\Header::class, 'Dynamic scroll in Grid with fixed column headers']);
//$c = $app->add(\Atk4\Ui\Layout\Column::class);
//$c = \Atk4\Ui\Layout\Column::addTo($app);
//$c1 = $c->addColumn('test');
/** $g1 CRUDExtension */
//$g1 = $c1->add([new CRUDExtension(),]);
$g1 = CRUDExtension::addTo($app);
$g1->lowercasePrefix = 'lwcs_';
//$g1 = $c1->add(['crud']);
$m1 = $g1->setModel(new Country($app->db)); //, ['name', 'iso']);
// demo for additional action buttons in CRUD + jsPaginator
//$g1->addModalAction(['icon'=>'cogs'], 'Details', function ($p, $id) use ($g1) {
//    $p->add(['Card'])->setModel($g1->model->load($id));
//});
//$g1->addAction('red', function ($js) {
//    return $js->closest('tr')->css('color', 'red');
//});

//$r1 = new \atk4\ui\jsReload($header, ['p1' => new \atk4\ui\jsExpression('row.getData().id')]);
//
//$g1->tabulatorOptions['rowClick'] = new \atk4\ui\jsFunction(['e', 'row'], [
//    $r1,
//]);

//$t = $app->stickyGet('haha');
//$g1->add([\Atk4\Ui\Text::class, $t]);
//$g1->addAction('akcijas nosaukums', function($j) {
//
//});
//$g1->setColumnAsHtml('name');
$g1->addQuickSearch(['iso', 'name']);
$g1->addDynamicColumn('append', 'Append', function($model) {
    return $model['iso'] . '_appended';
});
$selection = $g1->addSelection();
$g1->menu->addItem('show selection')->on('click', new \atk4\ui\jsExpression(
    'alert("Selected: "+[])', [$selection]
));
$app->requireJS('http://oss.sheetjs.com/js-xlsx/xlsx.full.min.js');
$g1->menu->addItem('Download xlsx')->on('click', new \atk4\ui\jsExpression(
    $g1->getTabulatorVariable() . '.download("xlsx", "data.xlsx", {sheetName:"Sheet name"})'
));

$g1->headerFilter(['append']);
//$g1->addCalculation('numcode', function() use ($app) {
//    $app->terminate(10);
//});

$g1->tabulatorOptions['columns']['iso']['headerFilter'] = 'autocomplete';
$g1->tabulatorOptions['columns']['iso']['headerFilterParams']['values'] = new \atk4\ui\jsFunction(['a'], [
    new \atk4\ui\jsExpression('return {"aa": "aa"}'),
]);
//$g1->headerSelectFilter('iso', function() use ($app) {
//    //querystring parametrs q satur meklēto frāzi
//    $q = $_GET['q'] ?? null;
//
//    if ($q) {
//        $app->terminate(json_encode(['NAV', 'LV', 'xx']));
//    } else {
//        $app->terminate(json_encode(['LV', 'LT', 'EE']));
//    }
//}, true);
$g1->headerSelectFilter('iso3', function() use ($app) {
    //querystring parametrs q satur meklēto frāzi
    $q = $_GET['q'] ?? null;

    if ($q) {
        $app->terminate(json_encode(['NAV', 'LV', 'xx']), ['content-type'=>'json']);
        exit;
    } else {
        $app->terminateJson(json_encode(['LV', 'LT', 'EE']), ['content-type'=>'json']);
        exit;
    }
}, true);
$g1->headerSelectFilter('name', function() use ($app) {
    //querystring parametrs q satur meklēto frāzi
    $q = $_GET['q'] ?? null;

    if ($q) {
        $app->terminate(json_encode(['NAV', 'LV', 'xx']), ['content-type'=>'json']);
    } else {
        $app->terminate(json_encode(['LV', 'LT', 'EE']), ['content-type'=>'json']);
    }
}, true);
$g1->setTabulatorOption('persistenceMode', true);
$g1->setTabulatorOption('persistentLayout', true);
$g1->setTabulatorOption('tooltipsHeader', new \atk4\ui\jsFunction(['column'], [
    new \atk4\ui\jsExpression('return column.getDefinition().title'),
]));
$g1->addJsPaginatorInContainer(20, '75vh');

// THIS SHOULD GO AFTER YOU CALL addAction() !!!
// $g1->addJsPaginatorInContainer(30, 350);
